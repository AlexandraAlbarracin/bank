package service;

import java.util.List;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import domain.Account;

public class cuentaService {

	AccountRepository repository;

	public cuentaService(AccountRepository repository) {
		this.repository = repository;
		this.crearcuentas();
	}

	public void crearcuentas() {
	
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		Account a3 = new Account("1003", 550);
		Account a4 = new Account("1004", 50);
		Account a5 = new Account("1005", 550);
		Account a6 = new Account("1006", 50);
		Account a7 = new Account("1007", 550);
		Account a8 = new Account("1008", 50);
		Account a9 = new Account("1009", 550);
		Account a10 = new Account("1010", 50);
		
		repository.save(a1);
		repository.save(a2);
		repository.save(a10);
		repository.save(a3);
		repository.save(a4);
		repository.save(a5);
		repository.save(a6);
		repository.save(a7);
		repository.save(a8);
		repository.save(a9);
	}
	
	public List<Account> getlist(){
		List<Account> ListaCuentas ;
		ListaCuentas = repository.findAll();
		return ListaCuentas;
	}
}
